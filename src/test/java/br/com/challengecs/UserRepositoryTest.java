/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package br.com.challengecs;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Calendar;
import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import br.com.challengecs.model.UserModel;
import br.com.challengecs.security.model.JwtUserDto;
import br.com.challengecs.security.util.JwtUtil;
import br.com.challengecs.service.UserModelService;

@RunWith(SpringRunner.class)
@SpringBootTest @ActiveProfiles("test")
@AutoConfigureMockMvc
public class UserRepositoryTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private UserModelService userModelService;
	
	@Autowired
	private JwtUtil jwtUtil;
	
	@Before
	public void insertRecordBeforeTests() throws Exception 
	{
		UserModel userModel = new UserModel();
		userModel.setCreatedDate(new Date());
		userModel.setModifiedDate(new Date());
		userModel.setLastLoginDate(new Date());
		userModel.setEmail("fabiano.costa.alvarenga@gmail.com");
		userModel.setName("Fabiano Costa de Alvarenga");
		userModel.setPassword("fabiano");
		userModel.setToken(generateTokenHelper());
		userModelService.save(userModel);
	}
	
	public String generateTokenHelper()
	{
		JwtUserDto jwtUserDto = new JwtUserDto();
		jwtUserDto.setPassword("fabiano");
		jwtUserDto.setUsername("fabiano.costa.alvarenga@gmail.com");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.SECOND, 60);
		jwtUserDto.setExpirtation(calendar.getTime());
		
		return jwtUtil.generateToken(jwtUserDto);
	}
	
	public JwtUserDto parseTokenHelper(String token)
	{
		return jwtUtil.parseToken(token);
	}
	
	@Test
	public void cadastroTest() throws Exception {

		UserModel userModel = userModelService.findByEmail("fabiano.costa.alvarenga@gmail.com");
		
		mockMvc.perform(post("/users/cadastro").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.content("{\"name\": \"Michelle Alvarenga\", \"email\": \"michelle.alvarenga@gmail.com\", \"password\": \"michelle\", \"phones\": [{\"ddd\": \"21\", \"number\": \"986126975\"}]}")
				.header("X-Authorization", userModel.getToken())
				).andExpect(status().isCreated())
				.andExpect(jsonPath("$.name").value("Michelle Alvarenga"))
				.andExpect(jsonPath("$.email").value("michelle.alvarenga@gmail.com"));
		
	}

	@Test
	public void loginTest() throws Exception 
	{
		mockMvc.perform(post("/users/login").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.content("{ \"username\": \"fabiano.costa.alvarenga@gmail.com\", \"password\": \"fabiano\"}")
				).andExpect(status().isOk());
	}

	@Test
	public void refresh_tokenTest() throws Exception 
	{
		MvcResult mvcResult = mockMvc.perform(post("/users/refresh-token").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.content("{ \"username\": \"fabiano.costa.alvarenga@gmail.com\", \"password\": \"fabiano\"}")
				).andExpect(status().isOk()).andReturn();
		
		Assert.assertNotNull(mvcResult.getResponse().getContentAsString());
	}

	@Test
	public void findByIdTest() throws Exception 
	{
		UserModel userModel = userModelService.findByEmail("fabiano.costa.alvarenga@gmail.com");
		
		mockMvc.perform(get("/users/"+userModel.getId()).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.content("{ \"username\": \"fabiano.costa.alvarenga@gmail.com\", \"password\": \"fabiano\"}")
				.header("X-Authorization", userModel.getToken())
				).andExpect(status().isOk())
		.andExpect(jsonPath("$.name").value("Fabiano Costa de Alvarenga"))
		.andExpect(jsonPath("$.email").value("fabiano.costa.alvarenga@gmail.com"));
	}

}
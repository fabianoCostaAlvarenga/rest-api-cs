package br.com.challengecs.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import br.com.challengecs.model.PhoneModel;


public interface PhoneRepository extends PagingAndSortingRepository<PhoneModel, String> {
	
}

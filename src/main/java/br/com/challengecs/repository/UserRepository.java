package br.com.challengecs.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.challengecs.model.UserModel;

public interface UserRepository extends JpaRepository<UserModel, String> 
{

	UserModel findByEmail(String email);
	
	UserModel findByToken(String token);
}

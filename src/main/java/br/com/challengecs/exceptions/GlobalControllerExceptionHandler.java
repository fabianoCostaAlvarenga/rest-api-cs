package br.com.challengecs.exceptions;

import java.util.HashSet;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import br.com.challengecs.util.ErrorMessageDTO;

@ControllerAdvice @RestController
public class GlobalControllerExceptionHandler
{

	private Set<ErrorMessageDTO> errorMessageDTOs;
	private ResponseEntity<?> response;
	
	@ExceptionHandler(value = Exception.class)
	public ResponseEntity<?> handleException(Exception e)
	{
		
		if(e instanceof ConstraintViolationException)
		{
			errorMessageDTOs = new HashSet<>();
			for (ConstraintViolation<?> violation : ((ConstraintViolationException)e).getConstraintViolations()) 
				errorMessageDTOs.add(new ErrorMessageDTO(violation.getPropertyPath().toString().toUpperCase() +" "+ violation.getMessage()));
			
			response = new ResponseEntity<>(errorMessageDTOs, HttpStatus.PRECONDITION_FAILED);
		}
		else 
		{
			errorMessageDTOs = new HashSet<>();
			errorMessageDTOs.add(new ErrorMessageDTO(e.getMessage()));
			response = new ResponseEntity<>(errorMessageDTOs, HttpStatus.BAD_REQUEST);
		}
		
		return response;
	}
	
}

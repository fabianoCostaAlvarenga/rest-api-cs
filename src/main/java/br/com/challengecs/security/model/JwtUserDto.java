package br.com.challengecs.security.model;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class JwtUserDto implements Serializable
{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;

    private String username;
    
    private String password;

    private String role;
    
    private Date expirtation;

    public JwtUserDto() {}
    
	public JwtUserDto(String id, String username, String password, String role) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.role = role;
	}

    
}
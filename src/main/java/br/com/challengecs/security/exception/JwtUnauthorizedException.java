package br.com.challengecs.security.exception;

import org.springframework.security.core.AuthenticationException;

public class JwtUnauthorizedException extends AuthenticationException 
{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public JwtUnauthorizedException(String msg) {
        super(msg);
    }
}

package br.com.challengecs.security.exception;

import org.springframework.security.core.AuthenticationException;

public class JwtInvalidTokenException extends AuthenticationException 
{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public JwtInvalidTokenException(String msg) {
        super(msg);
    }
}

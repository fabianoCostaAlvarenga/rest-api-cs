package br.com.challengecs.security.exception;

import org.springframework.security.core.AuthenticationException;

public class JwtExpirationTokenException extends AuthenticationException 
{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public JwtExpirationTokenException(String msg) {
        super(msg);
    }
}

package br.com.challengecs.security.util;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import br.com.challengecs.security.model.JwtUserDto;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtUtil 
{

	@Value("${jwt.secret}")
    private String secret;

	@Value("${jwt.expiration-time}")
    private Long expirationTime;

    public JwtUserDto parseToken(String token) 
    {
    	JwtUserDto u = null;
    	
        try {
            Claims body = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();

            u = new JwtUserDto();
            u.setUsername(body.getSubject());
            u.setId(body.get("userId").toString());
            u.setExpirtation(body.getExpiration());
            u.setRole((String) body.get("role"));

        } catch (JwtException | ClassCastException e) {
        	e.printStackTrace();
        }
        
        return u;
        
    }

    public String generateToken(JwtUserDto u) 
    {
        Claims claims = Jwts.claims().setSubject(u.getUsername());
        claims.put("userId", u.getId() + "");
        claims.put("role", u.getRole());
        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, secret)
                .setExpiration(new Date(System.currentTimeMillis() + expirationTime))
                .compact();
    }
    
}

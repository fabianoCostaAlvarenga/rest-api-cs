package br.com.challengecs.security;

import java.io.IOException;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import com.google.gson.Gson;

import br.com.challengecs.model.UserModel;
import br.com.challengecs.security.exception.JwtExpirationTokenException;
import br.com.challengecs.security.exception.JwtInvalidTokenException;
import br.com.challengecs.security.exception.JwtTokenMalformedException;
import br.com.challengecs.security.exception.JwtTokenMissingException;
import br.com.challengecs.security.exception.JwtUnauthorizedException;
import br.com.challengecs.security.model.JwtAuthenticationToken;
import br.com.challengecs.security.model.JwtUserDto;
import br.com.challengecs.security.util.JwtUtil;
import br.com.challengecs.service.UserModelService;
import br.com.challengecs.util.ErrorMessageDTO;

public class JwtAuthenticationFilter extends AbstractAuthenticationProcessingFilter 
{

	@Value("${jwt.header}")
    public String jwtTokenHeaderParam;
	
    @Autowired
    private JwtUtil jwtUtil;
    
    @Autowired
    private UserModelService userService;

    private final String[] freeURL;
    
	public JwtAuthenticationFilter(String[] freeURL) 
	{
		super("/**");
        this.freeURL = freeURL;
	}
	
	@Override
	protected boolean requiresAuthentication(HttpServletRequest request, HttpServletResponse response) 
	{
		for (String string : freeURL) 
		{
			if(request.getRequestURL().toString().endsWith(string))
				return false;
		}
		
		return true;
	}
	
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException, IOException, ServletException 
	{
		String tokenInHeader = request.getHeader(this.jwtTokenHeaderParam);

		/* se token não existir no header */
        if (tokenInHeader == null) 
            throw new JwtTokenMissingException("Não autorizado!");

        JwtUserDto jwtUserDto = jwtUtil.parseToken(tokenInHeader);
        
        /* Se não conseguir extrair do token um username */
        if (null == jwtUserDto || StringUtils.isBlank(jwtUserDto.getUsername())) 
        	throw new JwtTokenMalformedException("Token JWT é inválido!");
        
        /* Se token expirou */
         if(jwtUserDto.getExpirtation().before(new Date()))
        	throw new JwtExpirationTokenException("Sessão Inválida!");
        
        UserModel userModel = userService.findByEmail(jwtUserDto.getUsername());
        
        /* Se usuário do token nãoo existir no modelo */
        if(null == userModel)
        	throw new JwtInvalidTokenException("Token JWT é inválido para este usuário!");
        
        /* Se token do header for diferente do token do modelo do usuário */
        if (!userModel.getToken().equals(tokenInHeader))
        	throw new JwtUnauthorizedException("Não autorizado!");
        
        JwtAuthenticationToken authRequest = new JwtAuthenticationToken(tokenInHeader);

        return getAuthenticationManager().authenticate(authRequest);
	}
	
	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws IOException, ServletException 
	{
		super.successfulAuthentication(request, response, chain, authResult);
		
		UserModel userModel = userService.findByEmail(authResult.getName());
		
		if(null != userModel)
			userModel.setLastLoginDate(new Date());
		
		userService.save(userModel);
		
		chain.doFilter(request, response);
	}
	
	@Override
	protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException failed) throws IOException, ServletException 
	{
		
		String json = new Gson().toJson(new ErrorMessageDTO(failed.getMessage()));
	    response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
		
		if(failed instanceof JwtTokenMissingException)
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		else if(failed instanceof JwtTokenMalformedException)
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		else if(failed instanceof JwtExpirationTokenException)
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		else if(failed instanceof JwtInvalidTokenException)
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		else if(failed instanceof JwtUnauthorizedException)
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

	    response.getWriter().write(json);
		
	}
}

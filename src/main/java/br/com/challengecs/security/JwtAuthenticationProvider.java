package br.com.challengecs.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import br.com.challengecs.security.exception.JwtTokenMalformedException;
import br.com.challengecs.security.model.AuthenticatedUser;
import br.com.challengecs.security.model.JwtAuthenticationToken;
import br.com.challengecs.security.model.JwtUserDto;
import br.com.challengecs.security.util.JwtUtil;

@Component
public class JwtAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider 
{
	
	@Autowired
	private JwtUtil jwtUtil;

	@Override
	public boolean supports(Class<?> authentication) 
	{
		return (JwtAuthenticationToken.class.isAssignableFrom(authentication));
	}
	
	@Override
	protected void additionalAuthenticationChecks(UserDetails userDetails,
			UsernamePasswordAuthenticationToken authentication) throws AuthenticationException { }
	
	@Override
	protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication)
			throws AuthenticationException 
	{
		JwtAuthenticationToken jwtAuthenticationToken = (JwtAuthenticationToken) authentication;
        String token = jwtAuthenticationToken.getToken();

        JwtUserDto parsedUser = jwtUtil.parseToken(token);

        if (parsedUser == null)
            throw new JwtTokenMalformedException("JWT token is not valid");

        //List<GrantedAuthority> authorityList = AuthorityUtils.commaSeparatedStringToAuthorityList(parsedUser.getRole());

        return new AuthenticatedUser(parsedUser.getId(), parsedUser.getUsername(), token, null); //authorityList);
	}
}

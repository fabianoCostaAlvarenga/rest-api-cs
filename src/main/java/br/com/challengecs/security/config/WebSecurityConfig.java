package br.com.challengecs.security.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import br.com.challengecs.security.JwtAuthenticationFilter;
import br.com.challengecs.security.JwtAuthenticationProvider;
import br.com.challengecs.security.RestAuthenticationEntryPoint;
import br.com.challengecs.security.handler.JwtAuthenticationSuccessHandler;

@Configuration
@EnableWebSecurity
@EnableAutoConfiguration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter 
{
	
    @Autowired    
    private RestAuthenticationEntryPoint unauthorizedHandler;

    @Autowired 
    private JwtAuthenticationProvider authenticationProvider;
    
    @Bean
    @Override
    public AuthenticationManager authenticationManager() throws Exception 
    {
        return new ProviderManager(Arrays.asList(authenticationProvider));
    }

    @Bean
    public JwtAuthenticationFilter authenticationTokenFilterBean() throws Exception 
    {
    	String[] freeUrls = {"/users/login","/users/refresh-token"};
    	JwtAuthenticationFilter authenticationTokenFilter = new JwtAuthenticationFilter(freeUrls);
        authenticationTokenFilter.setAuthenticationManager(authenticationManager());
        authenticationTokenFilter.setAuthenticationSuccessHandler(new JwtAuthenticationSuccessHandler());
        return authenticationTokenFilter;
    }
       
    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception 
    {
    	/* We don't need CSRF for JWT based authentication */
    	httpSecurity.csrf().disable();
    	httpSecurity.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    	httpSecurity.exceptionHandling().authenticationEntryPoint(this.unauthorizedHandler);
    	
    	/* URL login */
    	httpSecurity.authorizeRequests().antMatchers("/users/login").permitAll();
    	/* URL de atualização do Token */
    	httpSecurity.authorizeRequests().antMatchers("/users/refresh-token").permitAll(); 
    	/* URL protegidas da API */
    	httpSecurity.authorizeRequests().anyRequest().authenticated();

    	httpSecurity.addFilterBefore(authenticationTokenFilterBean(), UsernamePasswordAuthenticationFilter.class);
    	
    	/* Desabilita cache de página */
    	httpSecurity.headers().cacheControl();
    }
}

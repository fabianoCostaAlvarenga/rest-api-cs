package br.com.challengecs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity @Data
@Table(name="tb_phone")
@EqualsAndHashCode(callSuper = false)
public class PhoneModel extends AbstractEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@NotNull @NotBlank
	@Size(min = 8, max = 9)
	@Column(name = "number", length = 9)
	private String number;
	
	@NotNull @NotBlank
	@Size(min = 2, max = 2, message = "tamanho deve ser 2")
	@Column(name = "ddd", length = 2)
	private String ddd;
	
}

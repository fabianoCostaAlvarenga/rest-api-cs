package br.com.challengecs.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity @Data 
@Table(name="tb_user", 
		uniqueConstraints = {@UniqueConstraint(columnNames={"email"})} )
@EqualsAndHashCode(callSuper = false)
public class UserModel extends AbstractEntity 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String PATTERN_DATE = "yyyy-MM-dd";
	
	@Column(name = "name", length = 60, nullable = false)
	private String name;

	@Column(name = "email", length = 60, nullable = false)
	private String email;

	@Column(name = "password", nullable = false)
	private String password;

	@Column(name = "token")
	private String token;
	
	@JsonIgnore
	@Column(name = "created")
	@Temporal(TemporalType.DATE)
	private Date createdDate;

	@Transient
	private String created;
	
	@JsonIgnore
	@Column(name = "modified")
	@Temporal(TemporalType.DATE)
	private Date modifiedDate;
	
	@Transient
	private String modified;
	
	@JsonIgnore
	@Column(name = "last_login")
	@Temporal(TemporalType.DATE)
	private Date lastLoginDate;

	@Transient
	private String lastLogin;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "tb_user_phones", joinColumns = @JoinColumn(name = "user_id"),
    	inverseJoinColumns = @JoinColumn(name = "phone_id"))
	private List<PhoneModel> phones;
	
	@PrePersist
	public void prePersist()
	{
		/* Quando novo registro */
		if(this.getId() == null)
		{
			
			this.password = new BCryptPasswordEncoder().encode(this.password);
			
			/* seta a data corrente */
			this.createdDate = new Date();
			/* a data do último login será igual a da criação */
			this.lastLoginDate = this.createdDate;
		}	
	}
	
	@PreUpdate
	public void preUpdate()
	{
		/* seta data da última modificação do registro */
		this.modifiedDate = new Date();
	}
	
	public String getCreated()
	{
		return null != this.createdDate ? new SimpleDateFormat(PATTERN_DATE).format(this.createdDate) : null;
	}
	
	public String getModified()
	{
		return null != this.createdDate ? new SimpleDateFormat(PATTERN_DATE).format(this.modifiedDate) : null;
	}
	
	public String getLastLogin()
	{
		return null != this.createdDate ? new SimpleDateFormat(PATTERN_DATE).format(this.lastLoginDate) : null;
	}
	
}

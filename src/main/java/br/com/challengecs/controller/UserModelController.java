package br.com.challengecs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.challengecs.model.UserModel;
import br.com.challengecs.security.model.AuthenticatedUser;
import br.com.challengecs.security.model.JwtUserDto;
import br.com.challengecs.security.util.JwtUtil;
import br.com.challengecs.service.UserModelService;
import br.com.challengecs.util.ErrorMessageDTO;

@RestController
@RequestMapping(value = "/users", 
		produces = { MediaType.APPLICATION_JSON_UTF8_VALUE }, consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE })
public class UserModelController 
{

	@Value("${jwt.header}")
    public String jwtTokenHeaderParam;
	
	@Autowired
	private JwtUtil jwtUtil;
	
	@Autowired
	private UserModelService userService;
	
	@RequestMapping(value = "/cadastro", method = RequestMethod.POST)
	public ResponseEntity<?> save(@RequestBody UserModel user) 
	{
		ResponseEntity<?> response = null;
		
		/* Check if email exists */
		if(null != userService.findByEmail(user.getEmail()))
			response = new ResponseEntity<>(new ErrorMessageDTO("E-mail já existente!"), HttpStatus.PRECONDITION_FAILED);
		else
		{
			user.setToken("");
			userService.save(user);
			JwtUserDto jwtUserDto = new JwtUserDto(user.getId(), user.getEmail(), user.getPassword(), null);
			user.setToken(jwtUtil.generateToken(jwtUserDto));
			userService.save(user);
			response = new ResponseEntity<>(user, HttpStatus.CREATED);
		}
		
		return response;
	}

	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseEntity<?> login(@RequestBody JwtUserDto user) 
	{
		ResponseEntity<?> response = null;
		UserModel userModel = userService.findByEmail(user.getUsername());
		
		if(null != userModel)
		{
			if(BCrypt.checkpw(user.getPassword(), userModel.getPassword()))
				response = new ResponseEntity<>(userModel, HttpStatus.OK);
			else
				response = new ResponseEntity<>(new ErrorMessageDTO("Usuário e/ou senha inválidos!"), HttpStatus.UNAUTHORIZED);
		}	
		else
		{
			response = new ResponseEntity<>(new ErrorMessageDTO("Usuário e/ou senha inválidos!"), HttpStatus.UNAUTHORIZED);
		}	
			
		return response;
	}
	
	@RequestMapping(value = "/refresh-token", method = RequestMethod.POST)
	public ResponseEntity<?> refresh_token(@RequestBody JwtUserDto user) 
	{
		ResponseEntity<?> response = null;
		UserModel userModel = userService.findByEmail(user.getUsername());
		
		if(null != userModel)
		{
			if(BCrypt.checkpw(user.getPassword(), userModel.getPassword()))
			{
				JwtUserDto jwtUserDto = new JwtUserDto(user.getId(), userModel.getEmail(), user.getPassword(), null);
				userModel.setToken(jwtUtil.generateToken(jwtUserDto));
				userService.save(userModel);
				response = new ResponseEntity<>(userModel, HttpStatus.OK);
			}	
			else
			{
				response = new ResponseEntity<>(new ErrorMessageDTO("Usuário e/ou senha inválidos!"), HttpStatus.UNAUTHORIZED);
			}	
		}	
		else
		{
			response = new ResponseEntity<>(new ErrorMessageDTO("Usuário e/ou senha inválidos!"), HttpStatus.UNAUTHORIZED);
		}	
			
		return response;
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> findById(@PathVariable ("id") String id) 
	{
		ResponseEntity<?> response = null;
		UserModel userModel = userService.findOne(id);
		
		if(null != userModel)
		{
			/* Se token do modelo igual ao token da sessão, então retorna o registro solicitado */
			AuthenticatedUser authenticatedUser = (AuthenticatedUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			if(userModel.getToken().equals(authenticatedUser.getToken()))
				response = new ResponseEntity<>(userModel, HttpStatus.OK);
			else
				response = new ResponseEntity<>(new ErrorMessageDTO("Não Autorizado!"), HttpStatus.UNAUTHORIZED);
		}	
		else
		{
			response = new ResponseEntity<>(new ErrorMessageDTO("Usuário não encontrado!"), HttpStatus.UNAUTHORIZED);
		}	
		
		return response;
	}
	
}

package br.com.challengecs.service;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.challengecs.model.UserModel;
import br.com.challengecs.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;

@Service("userService") @Slf4j
public class UserModelService implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	private UserRepository repository;
	
	public UserModel save(UserModel user)
	{
		UserModel userModel = null;
		try {
			userModel = repository.save(user);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return userModel;
	}
	
	public UserModel findByEmail(String email)
	{
		return repository.findByEmail(email);
	}
	
	public UserModel findOne(String id)
	{
		return repository.findOne(id);
	}

	public void deleteAll()
	{
		repository.deleteAll();
	}
	
}
